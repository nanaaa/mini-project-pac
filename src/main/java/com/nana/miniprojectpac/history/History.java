package com.nana.miniprojectpac.history;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class History {
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String noRekening;
    private Double jumlah;
    private String tipeTransaksi;

    public Long getId() {
        return id;
    }
    public void setId(Long id) {
        this.id = id;
    }
    public String getNoRekening() {
        return noRekening;
    }
    public void setNoRekening(String noRekening) {
        this.noRekening = noRekening;
    }
    public Double getJumlah() {
        return jumlah;
    }
    public void setJumlah(Double jumlah) {
        this.jumlah = jumlah;
    }
    public String getTipeTransaksi() {
        return tipeTransaksi;
    }
    public void setTipeTransaksi(String tipeTransaksi) {
        this.tipeTransaksi = tipeTransaksi;
    }
}
