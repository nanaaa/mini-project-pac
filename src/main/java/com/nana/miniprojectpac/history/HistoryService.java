package com.nana.miniprojectpac.history;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class HistoryService {
    
    @Autowired
    HistoryRepository historyRepository;

    public void createTransactionHistory(String noRekening, Double jumlah, String tipe) {

        History history = new History();
        history.setNoRekening(noRekening);
        history.setJumlah(jumlah);
        history.setTipeTransaksi(tipe);

        historyRepository.save(history);
    }
}
