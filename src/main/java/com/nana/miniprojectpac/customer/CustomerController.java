package com.nana.miniprojectpac.customer;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class CustomerController {
    
    @Autowired
    CustomerService customerService;

    @PostMapping("/customers/create")
    public ResponseEntity<CustomerResponseDTO> createCustomer(@RequestBody CustomerRequestDTO customerRequestDTO) {
        
        Customer result = customerService.createCustomer(customerRequestDTO);
        
        CustomerResponseDTO customerResponseDTO = new CustomerResponseDTO();
        customerResponseDTO.setNama(result.getNama());
        customerResponseDTO.setNik(result.getNik());
        customerResponseDTO.setNoTelp(result.getNoTelp());
        customerResponseDTO.setSaldo(result.getSaldo());
        customerResponseDTO.setNoRek(result.getNoRekening());
        customerResponseDTO.setStatus(result.getStatus());

        return ResponseEntity.status(HttpStatus.CREATED).body(customerResponseDTO);
    }

    @PutMapping("/customers/approve")
    public ResponseEntity<CustomerResponseDTO> processRegistration(@RequestBody RegistrationRequestDTO registrationRequestDTO) { 
        
        Customer result = customerService.processRegistration(registrationRequestDTO);

        CustomerResponseDTO customerResponseDTO = new CustomerResponseDTO();
        customerResponseDTO.setNama(result.getNama());
        customerResponseDTO.setNik(result.getNik());
        customerResponseDTO.setNoTelp(result.getNoTelp());
        customerResponseDTO.setSaldo(result.getSaldo());
        customerResponseDTO.setNoRek(result.getNoRekening());
        customerResponseDTO.setStatus(result.getStatus());

        return ResponseEntity.status(HttpStatus.OK).body(customerResponseDTO);
    }

    @PutMapping("/customers/{noRek}/withdraw")
    public ResponseEntity<CustomerResponseDTO> withdrawFund(@PathVariable String noRek, @RequestBody TransactionDTO transactionDTO) { 

        Customer result = customerService.withdrawFund(noRek, transactionDTO.getJumlah());

        CustomerResponseDTO customerResponseDTO = new CustomerResponseDTO();
        customerResponseDTO.setNama(result.getNama());
        customerResponseDTO.setNik(result.getNik());
        customerResponseDTO.setNoTelp(result.getNoTelp());
        customerResponseDTO.setSaldo(result.getSaldo());
        customerResponseDTO.setNoRek(result.getNoRekening());
        customerResponseDTO.setStatus(result.getStatus());

        return ResponseEntity.status(HttpStatus.OK).body(customerResponseDTO);
    }

    @PutMapping("/customers/{noRek}/deposit")
    public ResponseEntity<CustomerResponseDTO> depositFund(@PathVariable String noRek, @RequestBody TransactionDTO transactionDTO) { 

        Customer result = customerService.depositFund(noRek, transactionDTO.getJumlah());

        CustomerResponseDTO customerResponseDTO = new CustomerResponseDTO();
        customerResponseDTO.setNama(result.getNama());
        customerResponseDTO.setNik(result.getNik());
        customerResponseDTO.setNoTelp(result.getNoTelp());
        customerResponseDTO.setSaldo(result.getSaldo());
        customerResponseDTO.setNoRek(result.getNoRekening());
        customerResponseDTO.setStatus(result.getStatus());

        return ResponseEntity.status(HttpStatus.OK).body(customerResponseDTO);
    }

    @GetMapping("/customers/{noRek}/inquire")
    public String inquire(@PathVariable String noRek) {

        Customer result = customerService.findCustomerByNoRek(noRek);

        return "Saldo Anda saat ini sejumlah Rp" + result.getSaldo();
    }
}
