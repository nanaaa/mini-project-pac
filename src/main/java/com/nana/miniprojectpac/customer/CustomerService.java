package com.nana.miniprojectpac.customer;

import java.util.Random;

import com.nana.miniprojectpac.history.HistoryService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CustomerService {
    
    @Autowired
    CustomerRepository customerRepository;

    @Autowired
    HistoryService historyService;

    public Customer createCustomer(CustomerRequestDTO customerRequestDTO) {

        if(findCustomerByNoTelp(customerRequestDTO.getNoTelp()) != null) {
            throw new CustomerException("Nasabah dengan no. telepon " + customerRequestDTO.getNoTelp() + " sudah terdaftar.");
        }

        if(findCustomerByNik(customerRequestDTO.getNik()) != null) {
            throw new CustomerException("Nasabah dengan NIK " + customerRequestDTO.getNik() + " sudah terdaftar.");
        }

        Customer customer = new Customer();

        customer.setNama(customerRequestDTO.getNama());
        customer.setNoTelp(customerRequestDTO.getNoTelp());
        customer.setNik(customerRequestDTO.getNik());
        customer.setSaldo(0.0);
        customer.setStatus("PENDING");

        return customerRepository.save(customer);
    }

    public Customer findCustomerByNoTelp(String noTelp) {

        return customerRepository.findCustomerByNoTelp(noTelp);
    }

    public Customer findCustomerByNik(String nik) {

        return customerRepository.findCustomerByNik(nik);
    }

    public Customer findCustomerByNoRek(String noRek) {

        return customerRepository.findCustomerByNoRek(noRek);
    }

    public Customer processRegistration(RegistrationRequestDTO registrationRequestDTO) {

        Customer processedCustomer = customerRepository.findCustomerByNik(registrationRequestDTO.getNik());

        if (processedCustomer == null) {
            throw new CustomerException("Nasabah dengan NIK " + registrationRequestDTO.getNik() + " tidak ditemukan.");
        }

        if(registrationRequestDTO.getApproval().equals("APPROVE")) {

            if(processedCustomer.getStatus().equals("APPROVED")) {
                throw new CustomerException("Nasabah dengan NIK " + registrationRequestDTO.getNik() + " sudah di-approve.");
            } else {
                Random r = new Random( System.currentTimeMillis() );
                
                processedCustomer.setNoRekening("01" + Integer.toString((1 + r.nextInt(2)) * 100000 + r.nextInt(100000)));
                processedCustomer.setStatus("APPROVED");
                return customerRepository.save(processedCustomer);
            }
        } else if (registrationRequestDTO.getApproval().equals("REJECT")) {
            if(processedCustomer.getStatus().equals("APPROVED")) {
                throw new CustomerException("Nasabah dengan NIK " + registrationRequestDTO.getNik() + " sudah di-approve.");
            } else {
                processedCustomer.setStatus("REJECTED");
                return customerRepository.save(processedCustomer);
            }
        } else {
            throw new CustomerException("Harap diisi dengan APPROVE atau REJECT.");
        }
    }

    public Customer withdrawFund(String noRek, Double jumlah) {
        
        Customer processedCustomer = findCustomerByNoRek(noRek);

        if(processedCustomer.getSaldo() < jumlah) {
            throw new CustomerException("Saldo nasabah tidak mencukupi: Rp" + processedCustomer.getSaldo());
        }
        processedCustomer.setSaldo(processedCustomer.getSaldo() - jumlah);
        historyService.createTransactionHistory(noRek, jumlah, "TT");
        
        return customerRepository.save(processedCustomer);
    }

    public Customer depositFund(String noRek, Double jumlah) {

        Customer processedCustomer = findCustomerByNoRek(noRek);

        if(jumlah < 1000) {
            throw new CustomerException("Harap isi jumlah saldo setidaknya Rp1.000.");
        }
        processedCustomer.setSaldo(processedCustomer.getSaldo() + jumlah);
        historyService.createTransactionHistory(noRek, jumlah, "ST");
        
        return customerRepository.save(processedCustomer);
    }
}
