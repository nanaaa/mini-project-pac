package com.nana.miniprojectpac.customer;

public class CustomerException extends RuntimeException {
    
    private String message;
 
    public CustomerException() {}
 
    public CustomerException(String msg)
    {
        super(msg);
        this.message = msg;
    }
}
