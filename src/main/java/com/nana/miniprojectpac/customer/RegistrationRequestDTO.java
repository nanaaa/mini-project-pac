package com.nana.miniprojectpac.customer;

public class RegistrationRequestDTO {
    
    private String nik;
    private String approval;

    public String getNik() {
        return nik;
    }
    public void setNik(String nik) {
        this.nik = nik;
    }
    public String getApproval() {
        return approval;
    }
    public void setApproval(String approval) {
        this.approval = approval;
    }

}
