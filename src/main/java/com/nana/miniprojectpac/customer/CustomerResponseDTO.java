package com.nana.miniprojectpac.customer;

public class CustomerResponseDTO {
    
    private String nama;
    private String noTelp;
    private String nik;
    private Double saldo;
    private String noRek;
    private String status;

    public String getNama() {
        return nama;
    }
    public void setNama(String nama) {
        this.nama = nama;
    }
    public String getNoTelp() {
        return noTelp;
    }
    public void setNoTelp(String noTelp) {
        this.noTelp = noTelp;
    }
    public String getNik() {
        return nik;
    }
    public void setNik(String nik) {
        this.nik = nik;
    }
    
    public Double getSaldo() {
        return saldo;
    }
    public void setSaldo(Double saldo) {
        this.saldo = saldo;
    }    
    public String getNoRek() {
        return noRek;
    }
    public void setNoRek(String noRek) {
        this.noRek = noRek;
    }
    public String getStatus() {
        return status;
    }
    public void setStatus(String status) {
        this.status = status;
    }
}
