package com.nana.miniprojectpac.customer;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface CustomerRepository extends JpaRepository<Customer, Long> {
    
    @Query("select c from Customer c where c.noTelp = ?1")
    Customer findCustomerByNoTelp(String noTelp);

    @Query("select c from Customer c where c.nik = ?1")
    Customer findCustomerByNik(String nik);

    @Query("select c from Customer c where c.noRekening = ?1")
    Customer findCustomerByNoRek(String noRek);
}
